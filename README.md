keyhac config
=============

[keyhac](http://sites.google.com/site/craftware/keyhac)
の自分用設定ファイル保管場所。

補足
----
* 別途CapsLock=>右Ctrlの置き換えを行う必要あり

主な機能
--------
* Emacsライクなキーバインド
* Win+Alt+方向キーでAero Snapっぽい動作

動作確認環境
------------
* Windows 8.1
* keyhac 1.61
