# -*- coding: utf-8 -*-
import keyhac
import pyauto
import ckit.ckit_misc
import os.path
import re


def configure(keymap):

    # --------------------------------------------------------------------
    # config.py編集用のテキストエディタ
    if 1:
        from os import getenv
        from itertools import product
        search_dirs = (
                'C:\Applications',
                str(getenv('HOME')) + '\Applications',
                str(getenv('PROGRAMFILES')),
                str(getenv('PROGRAMFILES(X86)')))
        search_editors = (
                'vim74-kaoriya-win64\gvim.exe',
                'vim74-kaoriya-win32\gvim.exe',
                'sakura\sakura.exe')
        candidate_dirs = (x for x in search_dirs if os.path.exists(x))
        for i in product(candidate_dirs, search_editors):
            app_path = os.path.join(i[0], i[1])
            if os.path.exists(app_path):
                keymap.editor = app_path
                break

    # --------------------------------------------------------------------

    ctrl1 = lambda key: 'LC-' + key
    ctrl2 = lambda key: 'RC-' + key

    if 0:
        # キーバインド対象のCtrlキーを左右切り替え
        ctrl1, ctrl2 = ctrl2, ctrl1

    if 1:
        # 特定キー操作を無効化
        def do_nothing():
            pass
        km_global = keymap.defineWindowKeymap()
        km_global['F1'] = do_nothing
        km_global['Insert'] = do_nothing

    if 1:
        # F2キーを個人的に多用するので半角/全角キーをF2キーに置き換え
        keymap.replaceKey('(243)', 'F2')
        keymap.replaceKey('(244)', 'F2')

    # Emacs風のキー入力を割り当て
    if 1:
        def is_genuine_editor(window):
            return window.getClassName() in ('Emacs', 'Vim')

        def is_jvm_console(window):
            is_console_window = window.getClassName() == 'ConsoleWindowClass'
            text = window.getText()
            is_lein_repl = re.search('lein +repl$', text) != None
            is_sbt = re.search('sbt$', text) != None
            return is_console_window and (is_lein_repl or is_sbt)

        def ignore(window):
            return is_genuine_editor(window) or is_jvm_console(window)

        km_emacslike = keymap.defineWindowKeymap(
            check_func=lambda x: not ignore(x))

        km_emacslike[ctrl1('Q')] = 'A-F4'

        km_emacslike[ctrl2('A')] = 'Home'
        km_emacslike[ctrl2('B')] = 'Left'
        km_emacslike[ctrl2('D')] = 'Delete'
        km_emacslike[ctrl2('E')] = 'End'
        km_emacslike[ctrl2('F')] = 'Right'
        km_emacslike[ctrl2('G')] = 'Escape'
        km_emacslike[ctrl2('H')] = 'Back'
        km_emacslike[ctrl2('I')] = 'Tab'
        km_emacslike[ctrl2('K')] = 'S-End', 'C-X'
        km_emacslike[ctrl2('M')] = 'Enter'
        km_emacslike[ctrl2('N')] = 'Down'
        km_emacslike[ctrl2('P')] = 'Up'
        km_emacslike[ctrl2('S')] = 'C-F'
        km_emacslike[ctrl2('V')] = 'PageDown'
        km_emacslike[ctrl2('W')] = 'C-X'
        km_emacslike[ctrl2('X')] = keymap.defineMultiStrokeKeymap(ctrl2('X'))
        km_emacslike[ctrl2('X')]['H'] = 'C-A'
        km_emacslike[ctrl2('X')]['K'] = 'C-W'
        km_emacslike[ctrl2('X')]['OpenBracket'] = 'C-Home'
        km_emacslike[ctrl2('X')]['CloseBracket'] = 'C-End'
        km_emacslike[ctrl2('X')][ctrl2('C')] = 'A-F4'
        km_emacslike[ctrl2('X')][ctrl2('F')] = 'C-O'
        km_emacslike[ctrl2('X')][ctrl2('S')] = 'C-S'
        km_emacslike[ctrl2('Y')] = 'C-V'
        km_emacslike[ctrl2('W')] = 'C-X'
        km_emacslike[ctrl2('Slash')] = 'C-Z'
        km_emacslike['A-B'] = 'C-Left'
        km_emacslike['A-F'] = 'C-Right'
        km_emacslike['A-V'] = 'PageUp'
        km_emacslike['A-W'] = 'C-C'
        km_emacslike['A-Slash'] = 'C-Y'
        km_emacslike['A-S-Comma'] = 'C-Home'
        km_emacslike['A-S-Period'] = 'C-End'

    # Emacs用にWindowsキー入力を割り当て
    if 1:
        km_emacs = keymap.defineWindowKeymap(class_name='Emacs')

        km_emacs[ctrl1('A')] = 'C-X', 'H'
        km_emacs[ctrl1('C')] = 'A-W'
        km_emacs[ctrl1('F')] = 'C-S'
        km_emacs[ctrl1('O')] = 'C-X', 'C-F'
        km_emacs[ctrl1('S')] = 'C-X', 'C-S'
        km_emacs[ctrl1('W')] = 'C-X', 'K'
        km_emacs[ctrl1('X')] = 'C-W'
        km_emacs[ctrl1('V')] = 'C-Y'
        km_emacs[ctrl1('Z')] = 'C-Slash'
        km_emacs[ctrl1('Y')] = 'A-Slash'

    # Esc、C-[で無変換キーを押下
    if 1:
        km_imeoff = keymap.defineWindowKeymap()
        km_imeoff['Escape'] = 'Escape', '(29)'
        km_imeoff['C-OpenBracket'] = 'C-OpenBracket', '(29)'

    # Excel
    if 1:
        km_excel = keymap.defineWindowKeymap(
            check_func=lambda x: x.getClassName() in ('EXCEL6', 'EXCEL7'))
        # Alt+左右キーをCtrl+PageUp/Down(シート移動)に置き換え
        km_excel['A-Left'] = 'C-PageUp'
        km_excel['A-Right'] = 'C-PageDown'
        # Alt+上下キーをCtrl+Home/Endに置き換え
        km_excel['A-Up'] = 'C-Home'
        km_excel['A-Down'] = 'C-End'

    # Metasequoia
    if 1:
        km_metaseq = keymap.defineWindowKeymap(class_name='TFormMain')
        km_metaseq['F1'] = 'F1'

    # fenrir
    if 1:
        fenrir_exe = 'C:/Applications/fenrir/fenrir.exe'

        def exec_fenrir():
            if os.path.exists(fenrir_exe):
                fenrir_home = ckit.ckit_misc.splitPath(fenrir_exe)[0]
                pyauto.shellExecute('open', fenrir_exe, None, fenrir_home)

        km_fenrir = keymap.defineWindowKeymap()
        km_fenrir[ctrl1('Space')] = exec_fenrir

    # ウィンドウサイズを揃える
    if 1:
        from functools import partial
        from re import findall

        # リストの数値が丸め誤差の範囲内ならTrue、そうでないならFalseを返す
        def list_sames(lhs, rhs, round_=16):
            if len(lhs) != len(rhs):
                return False
            for (l, r) in zip(lhs, rhs):
                if l < r - round_ or r + round_ < l:
                    return False
            return True

        def aerosnap_toggle(directions, scales=['1/2', '1/3', '2/3']):
            w = pyauto.Window.getForeground()
            if w is None:
                return

            dirs = findall(r'[RDLU]', directions)
            if not dirs:
                # 上下左右が指定にない場合は最大化/元に戻すでトグル
                if w.isMaximized():
                    w.restore()
                else:
                    w.maximize()
            elif scales:
                # 最大化/最小化状態を解除
                is_restored = False
                if w.isMinimized() or w.isMaximized():
                    w.restore()
                    is_restored = True

                [[_, work_rect, _]] = pyauto.Window.getMonitorInfo()

                def gen_scaled_rect(scale):
                    [numer, denom] = [int(x) for x in scale.split('/')]

                    rect = work_rect[:]

                    win_width = (rect[2] - rect[0]) * numer // denom
                    if 'R' in dirs:
                        rect[0] = rect[2] - win_width
                    elif 'L' in dirs:
                        rect[2] = rect[0] + win_width

                    # 縦方向のサイズ変更は1/2のみとする
                    win_height = (rect[3] - rect[1]) // 2
                    if 'D' in dirs:
                        rect[1] = rect[3] - win_height
                    elif 'U' in dirs:
                        rect[3] = rect[1] + win_height

                    return rect

                # 拡大率リストからウィンドウサイズ指定先候補を取得
                rects = list(map(gen_scaled_rect, scales))

                if is_restored:
                    # 最大化/最小化からの復帰の場合は1番目の候補を使う
                    w.setRect(rects[0])
                else:
                    # 現在のウィンドウサイズが指定先候補リストに存在するものと
                    # 合致する場合、次の候補を使う
                    for i in range(0, len(rects) - 1):
                        prev, next_ = rects[i], rects[i + 1]
                        if list_sames(prev, w.getRect()):
                            w.setRect(next_)
                            break
                    else:
                        w.setRect(rects[0])

        km_aerosnap = keymap.defineWindowKeymap()
        for d in ('Left', 'Up', 'Right', 'Down', 'F'):
            km_aerosnap['W-A-' + d] = partial(aerosnap_toggle, d)
        km_aerosnap[ctrl1('W-A-Up')] = partial(aerosnap_toggle, 'LeftUp')
        km_aerosnap[ctrl1('W-A-Right')] = partial(aerosnap_toggle, 'RightUp')
        km_aerosnap[ctrl1('W-A-Left')] = partial(aerosnap_toggle, 'LeftDown')
        km_aerosnap[ctrl1('W-A-Down')] = partial(aerosnap_toggle, 'RightDown')

    # アクティブなエクスプローラで開いているフォルダをコマンドプロンプト開く
    if 0:
        ckw_exe = 'C:/Applications/ckw/ckw.exe'
        use_ckw = True

        from comtypes import client
        from ctypes import WinError
        from ctypes import windll
        import urllib
        from urlparse import urlparse

        GW_HWNDFIRST = 0
        GW_HWNDNEXT = 2
        NULL = 0
        ERROR_SUCCESS = 0

        def get_window(hwnd, ucmd):
            return_hwnd = windll.user32.GetWindow(hwnd, ucmd)
            if return_hwnd == NULL:
                if windll.kernel32.GetLastError() != ERROR_SUCCESS:
                    raise WinError()
            return return_hwnd

        # リスト内で条件を満たす最初の要素を探す
        def find_first(predicate, l):
            if not l:
                return None
            if predicate(l[0]):
                return l[0]
            return find_first(predicate, l[1:])

        # Zオーダーが指定したウィンドウと同じか下にあるフォルダを探す
        def find_first_folder(hwnd, folders):
            if hwnd == NULL:
                return None
            found = find_first((lambda x: x.HWND == hwnd), folders)
            if found is not None:
                return found
            return find_first_folder(get_window(hwnd, GW_HWNDNEXT), folders)

        def is_folder(window):
            return 'ShellFolder' in str(window.Document) and window.LocationURL

        # Zオーダーが一番上のエクスプローラで開いているフォルダのパスを探す
        def find_first_explorer_path():
            shell_app = client.CreateObject('Shell.Application')
            shell_folders = [x for x in shell_app.Windows() if is_folder(x)]
            if not shell_folders:
                return None

            hwnd_first = get_window(shell_folders[0].HWND, GW_HWNDFIRST)
            first_folder = find_first_folder(hwnd_first, shell_folders)
            if first_folder is None:
                return None

            return urllib.unquote(urlparse(first_folder.LocationURL).path)[1:]

        def open_cmd_with_first_explorer_path():
            # ジョブ起動でないとエラーが出る模様
            def job_1(job_item):
                # COMの初期化が必要
                windll.ole32.CoInitialize(0)
                try:
                    path = find_first_explorer_path()
                    if path is None:
                        # エクスプローラを開いていない場合はマイドキュメントを使う
                        shell = client.CreateObject('WScript.Shell')
                        path = shell.SpecialFolders('MyDocuments')
                    if use_ckw and os.path.exists(ckw_exe):
                        pyauto.shellExecute('open', ckw_exe, None, path)
                    else:
                        pyauto.shellExecute('open', 'cmd.exe', None, path)
                finally:
                    windll.ole32.CoUninitialize()

            def job_2(job_item):
                pass

            job_item = keyhac.JobItem(job_1, job_2)
            keyhac.JobQueue.defaultQueue().enqueue(job_item)

        km_open_cmd = keymap.defineWindowKeymap()
        km_open_cmd['W-A-Slash'] = open_cmd_with_first_explorer_path
